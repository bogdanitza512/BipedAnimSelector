﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
public class TrackballRotation : MonoBehaviour {

    #region Nested Types



    #endregion

    #region Fields and Properties

    /// <summary>
    /// The speed of rotational movement during mouse drag.
    /// </summary>
    [SerializeField, Tooltip("The speed of rotational movement.")]
    private float rotationSpeed;

    /// <summary>
    /// The axis of rotational movement during mouse drag.
    /// </summary>
    [SerializeField, Tooltip("The axis of rotational movement.")]
    private Vector3 rotationAxis;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    private void Start()
    {
	
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabledUpdate this instance.
    /// </summary>
    private void Update()
    {
	
    }

    /// <summary>
    /// OnMouseDrag is called when the user has clicked on a GUIElement or Collider and is still holding down the mouse
    /// </summary>
    private void OnMouseDrag()
    {
        float rotAmount = 
            Input.GetAxis("Mouse X") * rotationSpeed * Mathf.Deg2Rad;
        transform.Rotate(rotationAxis, rotAmount);
    }

    #endregion

    #region Methods



    #endregion

}
